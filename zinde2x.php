<!DOCTYPE html><html lang="en"><head><meta charset="utf-8"><title>VI-xmlParser</title><meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link href="src/bootstrap/css/bootstrap.darkly.min.css" rel="stylesheet">
    <link href="src/css/styles.css" rel="stylesheet">
</head>

<body>
<div class="container">
    <div id="Header" class="row"><div id="viheader" style="-moz-user-select: none; -webkit-user-select: none;
         -ms-user-select:none; user-select:none;-o-user-select:none;" unselectable="on" onselectstart="return false;" onmousedown="return false;">Vi RawUrlEnc</div></div>
    <br/><br/><br/>

    <?php
    $row1 = '<div class="row">';
    $row2 = '</div>';
    $ta1 = '<textarea cols="50" rows="4">';
    $ta2 = '</textarea>';
    $sp1 = '<span style="font-size:75px;">';
    $sp2 = '</span>';
    /*
        echo $row1.$sp1.'1'.$sp2.$ta1.rawurlencode($ss1).$ta2.$row2;
        echo $row1.$sp1.'2'.$sp2.$ta1.rawurlencode($ss2).$ta2.$row2;
        echo $row1.$sp1.'3'.$sp2.$ta1.rawurlencode($ss3).$ta2.$row2;
        echo $row1.$sp1.'4'.$sp2.$ta1.rawurlencode($ss4).$ta2.$row2;
        echo $row1.$sp1.'5'.$sp2.$ta1.rawurlencode($ss5).$ta2.$row2;
        echo $row1.$sp1.'6'.$sp2.$ta1.rawurlencode($ss6).$ta2.$row2;
    */
    $PLcode_above = file_get_contents('PLcode-above.php');
    $PLcode_below = file_get_contents('PLcode-below.htm');
    $s0             = '%3C%21doctype%20html%3E%3Chtml%3E%3Chead%3E%3Cmeta%20charset%3D%22utf-8%22%3E%20%3Cstyle%3Ebody%2C%20html%20%7Bbackground-color%3A%23000000%3Bborder%3Anone%3Bcolor%3A%23ffffff%3Boverflow%3Ahidden%3Bmargin%3A0px%3B%20height%3A100%25%3B%20width%3A100%25%3Bpadding%3A0%3B%7D%3C%2Fstyle%3E%3C%2Fhead%3E%3Cbody%3E%3C%2Fbody%3E%3C%2Fhtml%3E';
    $ss1            = file_get_contents('ss1.html');
    $ss2            = file_get_contents('ss2.html');
    $ss3            = file_get_contents('ss3.html');
    $ss4            = file_get_contents('ss4.html');
    $ss5            = file_get_contents('ss5.html');
    $ss6            = file_get_contents('ss6.html');
    $i_bern1        = file_get_contents('i_BernheimPPTforAvery.jpg.html');
    $i_bern2        = file_get_contents('i_BernheimPPTforAvery2.jpg.html');
    $i_brun1        = file_get_contents('i_Burnetts_1.jpg.html');
    $i_edrington1   = file_get_contents('i_Edrington_1.jpg.html');
    $i_edrington2   = file_get_contents('i_Edrington_2.jpg.html');
    $i_edrington3   = file_get_contents('i_Edrington_3.jpg.html');
    $i_edrington4   = file_get_contents('i_Edrington_4.jpg.html');
    $i_edrington5   = file_get_contents('i_Edrington_5.jpg.html');
    $i_edrington6   = file_get_contents('i_Edrington_6.jpg.html');
    $i_evan1        = file_get_contents('i_EvanW_AppleOrdchard_1.jpg.html'))  ;
    $v_bobpeters    = file_get_contents('v_ABCBobPeters_TitosKombuchaMule.ogv.html');
    $v_mgayblackb   = file_get_contents('v_ABCMountGayBlackBarrel.ogv.html');
    $v_mgaylesson   = file_get_contents('v_ABCMountGayRum_Lesson_MakeThePerfectRumPunch.ogv.html');
    $v_mgayorig     = file_get_contents('v_ABCMountGayRum_TheOriginalRum.ogv.html');
    $v_bac150       = file_get_contents('v_BACARDI150THTVSPOT.ogv.html');
    $v_dewars       = file_get_contents('v_DewarsnewpackageFlipTheSwitch.ogv.html');
    $v_goose1       = file_get_contents('v_Goosevid_A.ogv.html');
    $v_goose2       = file_get_contents('v_Goosevid_B.ogv.html');
    $v_goose3       = file_get_contents('v_Goosevid_C.ogv.html');
    $v_goose4       = file_get_contents('v_Goosevid_D.ogv.html');
    $v_jagerspice   = file_get_contents('v_JagerSpice.ogv.html');
    $v_makers46     = file_get_contents('v_MM_mm46_complicated.ogv.html');
    $v_captmorg1    = file_get_contents('v_Rumvid_A.ogv.html');
    $v_captmorg2    = file_get_contents('v_Rumvid_B.ogv.html');
    $v_captmorg3    = file_get_contents('v_Rumvid_C.ogv.html');

    $slide[0] =   [.001,$s0];
    $slide[1] =   [5   ,rawurlencode($ss1)];
    $slide[2] =   [18  ,rawurlencode($v_goose3)];
    $slide[3] =   [5   ,rawurlencode($i_edrington6)];
    $slide[4] =   [29  ,rawurlencode($v_makers46)];
    $slide[5] =   [9   ,rawurlencode($i_edrington5)];
    $slide[6] =   [10  ,rawurlencode($i_evan1)];
    $slide[7] =   [5   ,rawurlencode($ss2)];
    $slide[8] =   [19  ,rawurlencode($v_mgaylesson)];
    $slide[9] =   [7   ,rawurlencode($v_bac150)];
    $slide[10] =  [9   ,rawurlencode($i_edrington2)];
    $slide[11] =  [31  ,rawurlencode($v_captmorg3)];
    $slide[12] =  [5   ,rawurlencode($ss3)];
    $slide[13] =  [19  ,rawurlencode($v_goose4)];
    $slide[14] =  [9   ,rawurlencode($i_edrington1)];
    $slide[15] =  [34  ,rawurlencode($v_mgayblackb)];
    $slide[16] =  [20  ,rawurlencode($v_captmorg2)];
    $slide[17] =  [5   ,rawurlencode($ss4)];
    $slide[18] =  [9   ,rawurlencode($i_edrington4)];
    $slide[19] =  [31  ,rawurlencode($v_goose2)];
    $slide[20] =  [12  ,rawurlencode($i_bern2)];
    $slide[21] =  [12  ,rawurlencode($v_goose1)];
    $slide[22] =  [5   ,rawurlencode($ss5)];
    $slide[23] =  [9   ,rawurlencode($i_edrington3)];
    $slide[24] =  [17  ,rawurlencode($v_jagerspice)];
    $slide[25] =  [9   ,rawurlencode($i_edrington1)];
    $slide[26] =  [19  ,rawurlencode($v_mgayorig)];
    $slide[27] =  [5   ,rawurlencode($ss6)];
    $slide[28] =  [9   ,rawurlencode($i_bern1)];
    $slide[29] =  [94  ,rawurlencode($v_bobpeters)];
    $slide[30] =  [66  ,rawurlencode($v_dewars)];
    $slide[31] =  [29  ,rawurlencode($v_captmorg1)];
    $slide[32] =  [9   ,rawurlencode($i_brun1)];
    echo '<div class="row">';
    echo '<div class="col-sm-12">';
    echo '<pre cols="50" wrap="off" id="playlistpre">';
//    echo nl2br(htmlspecialchars($PLcode_above));
    echo '<br/><br/>';

    for($i=0;$i<=32;$i++){
        echo "PLtime[".$i."]  = ".($slide[$i][0]*1000).";<br/>";
    }
    echo "<br/><br/>";

    for($i=0;$i<=32;$i++){
        echo "PLcontent[".$i."]  = '".($slide[$i][1])."';<br/>";
    }
    echo "<br/><br/>";
//    echo nl2br(htmlspecialchars($PLcode_below));
    echo '<br/></pre>';
    echo '</div>';
    echo '</div>';
    ?>
</div>



<!--/////end:(BODY)/////-->


<!-- ==========JS=========== -->
<!--##JQUERY-->
<script src="src/js/jquery-1.11.3.min.js" type="text/javascript"></script>
<!--##BOOTSTRAP_3.3-->
<script src="src/bootstrap/js/bootstrap.min.js"></script>
<script>



</script>
</body>
</html>