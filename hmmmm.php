What to say in comfort stage:
"Wow, we used to be SO close. (cross your fingers) It's like we were hugging.
But now we've fallen apart (uncross your fingers, like your holding up a 'peace' sign).
Now I guess you just wanna hold hands."
Short, sweet, and works like a charm.
_________________
What to say when she starts acting up:
"Wow, how does your boyfriend put up with you?" - You
"Uh, I don't have a boyfriend." - Her
"Well, that's probably for the best!" - You
_________________
Check out my app on the Apple App Store:
Singled Out App - girls ask questions and eliminate guys based on how they respond.
Still need advice? You can email me here:
admin@setmeupinc.com
Last edited by Jay Wa on Sat Jun 28, 2008 4:38 pm, edited 3 times in total.
Top	 Profile
Jay Wa
Post subject: PostPosted: Wed Mar 26, 2008 8:55 pm
Offline
MPUA Forum Addict
User avatar
Joined: Fri Dec 28, 2007 3:56 am
Posts: 276
Location: Atlanta, GA (USA)
What to say at the introduction:
"So what's your name?" - You
"Jessica." - Her
'Wow! That's very unique. What is that like Turkish or something?" - You
_________________
Check out my app on the Apple App Store:
Singled Out App - girls ask questions and eliminate guys based on how they respond.
Still need advice? You can email me here:
admin@setmeupinc.com
Last edited by Jay Wa on Tue Apr 29, 2008 6:40 pm, edited 1 time in total.
Top	 Profile
kaala
Post subject: PostPosted: Wed Mar 26, 2008 10:37 pm
Offline
MPUA Forum Enthusiast
Joined: Sat Feb 23, 2008 10:10 pm
Posts: 73
Location: estonia
i have 2 routines that havnet been mentioned here before.
.
i tell her that im going to ask her 2 questions and she must answer ASAP.
me: what is the color of the fridge?
she: white
me: what does a cow drinks?
she: milk
3 seconds pause
she: ups, water (laughts)
another one
me: think of a 2-digit number between 1 and 50. both numbers are odd and cant be the same
me: is it 37?
its usually 37, but you can never be sure about it:)
Top	 Profile
God
Post subject: PostPosted: Thu Apr 10, 2008 2:17 pm
Jay Wa wrote:
Some more shit like this if she starts acting up.
"Wow, how does your boyfriend put up with you?" - You
"Uh, I don't have a boyfriend." - Her
"Well, that's probably for the best!" - Me
That one is good I'll remember that.
Top
Jay Wa
Post subject: PostPosted: Tue Apr 29, 2008 6:32 pm
Offline
MPUA Forum Addict
User avatar
Joined: Fri Dec 28, 2007 3:56 am
Posts: 276
Location: Atlanta, GA (USA)
What to say when she calls you out on something:
"Well, I thought I was being cute and funny. But I guess I was just being cute."
_________________
Check out my app on the Apple App Store:
Singled Out App - girls ask questions and eliminate guys based on how they respond.
Still need advice? You can email me here:
admin@setmeupinc.com
Last edited by Jay Wa on Thu May 01, 2008 3:49 pm, edited 2 times in total.
Top	 Profile
Jay Wa
Post subject: PostPosted: Tue Apr 29, 2008 6:36 pm
Offline
MPUA Forum Addict
User avatar
Joined: Fri Dec 28, 2007 3:56 am
Posts: 276
Location: Atlanta, GA (USA)
What to say if your in a big college town (works better if she is wearing her college gear):
"Holy shit! You go to UGA?" -You
"Yeah." -Her
"What a coincidence, so do I!!!" -You
The trick is to act really enthusiastic about it, like it's uncommon. I have never met a girl that didn't find it funny!
_________________
Check out my app on the Apple App Store:
Singled Out App - girls ask questions and eliminate guys based on how they respond.
Still need advice? You can email me here:
admin@setmeupinc.com
Last edited by Jay Wa on Sat Jun 28, 2008 4:38 pm, edited 4 times in total.
Top	 Profile
Jay Wa
Post subject: PostPosted: Tue Apr 29, 2008 6:39 pm
Offline
MPUA Forum Addict
User avatar
Joined: Fri Dec 28, 2007 3:56 am
Posts: 276
Location: Atlanta, GA (USA)
What to say when she calls:
"Hey, how are you?" - Her
"I'm good, how are you?" - You
"Good." - Her
"Wait, your good? (pause) I bet I could make you SO bad!" - You
Delivery is very important for this one.
_________________
Check out my app on the Apple App Store:
Singled Out App - girls ask questions and eliminate guys based on how they respond.
Still need advice? You can email me here:
admin@setmeupinc.com
Last edited by Jay Wa on Sat Jun 28, 2008 4:35 pm, edited 1 time in total.
Top	 Profile
Jay Wa
Post subject: PostPosted: Thu May 01, 2008 3:46 pm
Offline
MPUA Forum Addict
User avatar
Joined: Fri Dec 28, 2007 3:56 am
Posts: 276
Location: Atlanta, GA (USA)
What to say when she is nervous:
"Hey, can you do me a favor?" - You
"Um...sure." - Her
"Can you smile? It looks good on you." - You
_________________
Check out my app on the Apple App Store:
Singled Out App - girls ask questions and eliminate guys based on how they respond.
Still need advice? You can email me here:
admin@setmeupinc.com
Top	 Profile
Jay Wa
Post subject: PostPosted: Sat May 10, 2008 5:09 pm
Offline
MPUA Forum Addict
User avatar
Joined: Fri Dec 28, 2007 3:56 am
Posts: 276
Location: Atlanta, GA (USA)
What to say at the hook point:
"Have you ever met someone that you just feel so comfortable around? You know, you really hit it off with them? - You
"I totally know what you mean!" - Her
"Yeah...I've never met a person like that." (smile) - You
_________________
Check out my app on the Apple App Store:
Singled Out App - girls ask questions and eliminate guys based on how they respond.
Still need advice? You can email me here:
admin@setmeupinc.com
Last edited by Jay Wa on Sat Jun 28, 2008 4:39 pm, edited 1 time in total.
Top	 Profile
Okiokdan
Post subject: PostPosted: Sat May 10, 2008 6:21 pm
Offline
Member of MPUA Forum
Joined: Fri Nov 30, 2007 6:01 pm
Posts: 136
Jay Wa wrote:
What to say at the hook point:
"Have you ever met someone that you just feel so comfortable around? You know, you really hit it off with them? - You
"I totally know what you mean!" - Her
"Yeah...I've never met a person like that." (smile) - You
Awesome! More please  :D
Top	 Profile
MattJacks
Post subject: PostPosted: Mon May 12, 2008 1:05 pm
Offline
MPUA Forum Addict
User avatar
Joined: Tue Apr 29, 2008 2:21 am
Posts: 284
Location: Australia
Jay Wa wrote:
What to say when she is nervous:
"Hey, can you do me a favor?" - You
"Um...sure." - Her
"Can you smile? It looks good on you." - You
maybe as an opener aswell. if u can cold read some shy girl
Top	 Profile
Jay Wa
Post subject: PostPosted: Wed May 14, 2008 2:41 pm
Offline
MPUA Forum Addict
User avatar
Joined: Fri Dec 28, 2007 3:56 am
Posts: 276
Location: Atlanta, GA (USA)
What to say when she tries to bring you down:
"Why are you so happy?" - Her
"I don't know, but it's a lot of fun. You should try it out some time!"- You
_________________
Check out my app on the Apple App Store:
Singled Out App - girls ask questions and eliminate guys based on how they respond.
Still need advice? You can email me here:
admin@setmeupinc.com
Last edited by Jay Wa on Fri May 16, 2008 5:50 pm, edited 2 times in total.
Top	 Profile
Jay Wa
Post subject: PostPosted: Wed May 14, 2008 2:52 pm
Offline
MPUA Forum Addict
User avatar
Joined: Fri Dec 28, 2007 3:56 am
Posts: 276
Location: Atlanta, GA (USA)
What to say when she tells you she has a boyfriend:
"I have a boyfriend" - Her (in a playful way)
"I can totally relate...I actually have two kids." - You
She will more than likely call you out on it. But more importantly, she will be changing the subject back to you and probably realize that her being in a relationship is not that big of a deal.
"I have a boyfriend"- Her (in a defensive way)
"Wow, I'm sorry, you must have thought that I was hitting on you. I was actually just being nice..." - You
This will make her feel guilty and she will more than likely apologize to you.
"I have a boyfriend." - Her (in a bitchy way)
"Great. Can you do me a favor? Tell him to go fuck himself." - You
Use this last one only if you really have to.
