<!DOCTYPE html><html lang="en"><head><meta charset="utf-8"><title>VI-rawurlenc</title><meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link href="src/bootstrap/css/bootstrap.darkly.min.css" rel="stylesheet">
    <link href="src/css/styles.css" rel="stylesheet">
</head>

<body>

<div class="container">
    <div id="Header" class="row"><div id="viheader" style="-moz-user-select: none; -webkit-user-select: none;
         -ms-user-select:none; user-select:none;-o-user-select:none;" unselectable="on" onselectstart="return false;" onmousedown="return false;">Vi RawUrlEnc</div></div>
    <br/><br/><br/>
    <?php

    include 'inc_PLabove&below.php';
    include 'inc_img&vid_kit.php';
    include 'inc_specials.php';
    function rue($a){return rawurlencode($a);}

    $u1 = "http://vi-digital.com/lib/abc/";
    $u2 = "http://vi-digital.com/lib/abc/ABC-Cherry/";
    $u3 = "http://vi-digital.com/lib/vicorporatemenus/";
    $u4 = "http://vi-digital.com/lib/vicorporatemenus/ABC-Corp/";
    /*  i_ [0]TIME, [1]URL, [2]PATH, [3]bgsize*/

    /*  v_ [0]TIME, [1]URL, [2]PATH, [3]vIURL, [4]vIPATH, [5]VOLUME*/
    $i_bern1   = [9 ,$u2,'BernheimPPTforAvery.jpg', 'contain' ];
    $i_bern2   = [12,$u2,'BernheimPPTforAvery2.jpg', 'cover' ];
    $i_burn1   = [9 ,$u2,'Burnetts_1.jpg', 'contain' ];
    $i_edri1   = [9 ,$u2,'Edrington_1.jpg', 'contain' ];
    $i_edri2   = [9 ,$u2,'Edrington_2.jpg', 'contain' ];
    $i_edri3   = [9 ,$u2,'Edrington_3.jpg', 'contain' ];
    $i_edri4   = [9 ,$u2,'Edrington_4.jpg', 'contain' ];
    $i_edri5   = [9 ,$u2,'Edrington_5.jpg', 'contain' ];
    $i_edri6   = [5 ,$u2,'Edrington_6.jpg', 'contain' ];
    $i_evan1   = [9 ,$u2,'EvanW_AppleOrdchard_1.jpg', 'contain' ];
    $v_bac150  = [6 ,$u1,'BACARDI150THTVSPOT.ogv', Null, Null, 0.3];
    $v_dewars  = [66,$u1,'DewarsnewpackageFlipTheSwitch.ogv', Null, Null, Null];
    $v_goose1  = [12,$u2,'Goosevid_A.ogv', Null, Null, Null];
    $v_goose2  = [31,$u2,'Goosevid_B.ogv', Null, Null, Null];
    $v_goose3  = [18,$u2,'Goosevid_C.ogv', Null, Null, Null];
    $v_goose4  = [19,$u2,'Goosevid_D.ogv', Null, Null, Null];
    $v_captm1  = [29,$u2,'Rumvid_A.ogv', Null, Null, Null];
    $v_captm2  = [20,$u2,'Rumvid_B.ogv', Null, Null, Null];
    $v_captm3  = [31,$u2,'Rumvid_C.ogv', Null, Null, Null];
    $v_mgbbar  = [34,$u4,'ABCMountGayBlackBarrel.ogv', Null, Null, Null];
    $v_mgless  = [19,$u4,'ABCMountGayRum_Lesson_MakeThePerfectRumPunch.ogv', Null, Null, Null];
    $v_mgorig  = [19,$u4,'ABCMountGayRum_TheOriginalRum.ogv', Null, Null, Null];
    $v_jagspi  = [17,$u4,'JagerSpice.ogv', Null, Null, 0.3];
    $vi_bobpe  = [94,$u4,'ABCBobPeters_TitosKombuchaMule.ogv', $u3, 'ABCBobPeters_TitosKombuchaMule-vid-bg.png', Null];
    $vi_mmark  = [29,$u2,'MM_mm46_complicated.ogv', $u1, 'MM_mm46_complicated-vid-bg.png', Null];

    //    function mk_img($u, $filename, $bgsize)
//    function mk_vid($v_url, $v_filename, $i_url=Null, $i_filename=Null )

    $s0             = '%3C%21doctype%20html%3E%3Chtml%3E%3Chead%3E%3Cmeta%20charset%3D%22utf-8%22%3E%20%3Cstyle%3Ebody%2C%20html%20%7Bbackground-color%3A%23000000%3Bborder%3Anone%3Bcolor%3A%23ffffff%3Boverflow%3Ahidden%3Bmargin%3A0px%3B%20height%3A100%25%3B%20width%3A100%25%3Bpadding%3A0%3B%7D%3C%2Fstyle%3E%3C%2Fhead%3E%3Cbody%3E%3C%2Fbody%3E%3C%2Fhtml%3E';

    $slide[0] =   [.001,$s0];
    $slide[1] =   [24           ,rue($ss1)];
    $slide[2] =   [$v_goose3[0] ,rue(mk_vid($v_goose3))];
    $slide[3] =   [$i_edri6[0]  ,rue(mk_img($i_edri6))];
    $slide[4] =   [$vi_mmark[0] ,rue(mk_vid($vi_mmark))];
    $slide[5] =   [$i_edri5[0]  ,rue(mk_img($i_edri5))];
    $slide[6] =   [$i_evan1[0]  ,rue(mk_img($i_evan1))];
    $slide[7] =   [24           ,rue($ss2)];
    $slide[8] =   [$v_mgless[0] ,rue(mk_vid($v_mgless))];
    $slide[9] =   [$v_bac150[0] ,rue(mk_vid($v_bac150))];
    $slide[10] =  [$i_edri2[0]  ,rue(mk_img($i_edri2))];
    $slide[11] =  [$v_captm3[0] ,rue(mk_vid($v_captm3))];
    $slide[12] =  [24           ,rue($ss3)];
    $slide[13] =  [$v_goose4[0] ,rue(mk_vid($v_goose4))];
    $slide[14] =  [$i_edri1[0]  ,rue(mk_img($i_edri1))];
    $slide[15] =  [$v_mgbbar[0] ,rue(mk_vid($v_mgbbar))];
    $slide[16] =  [$v_captm2[0] ,rue(mk_vid($v_captm2))];
    $slide[17] =  [24           ,rue($ss4)];
    $slide[18] =  [$i_edri4[0]  ,rue(mk_img($i_edri4))];
    $slide[19] =  [$v_goose2[0] ,rue(mk_vid($v_goose2))];
    $slide[20] =  [$i_bern2[0]  ,rue(mk_img($i_bern2))];
    $slide[21] =  [$v_goose1[0] ,rue(mk_vid($v_goose1))];
    $slide[22] =  [24           ,rue($ss5)];
    $slide[23] =  [$i_edri3[0]  ,rue(mk_img($i_edri3))];
    $slide[24] =  [$v_jagspi[0] ,rue(mk_vid($v_jagspi))];
    $slide[25] =  [$i_edri1[0]  ,rue(mk_img($i_edri1))];
    $slide[26] =  [$v_mgorig[0] ,rue(mk_vid($v_mgorig))];
    $slide[27] =  [19           ,rue($ss6)];
    $slide[28] =  [$i_bern1[0]  ,rue(mk_img($i_bern1))];
    $slide[29] =  [$vi_bobpe[0] ,rue(mk_vid($vi_bobpe))];
    $slide[30] =  [$v_dewars[0] ,rue(mk_vid($v_dewars))];
    $slide[31] =  [$v_captm1[0] ,rue(mk_vid($v_captm1))];
    $slide[32] =  [$i_burn1[0]  ,rue(mk_img($i_burn1))];
    $numslides = count($slide);
    echo '<div class="row">';
    echo '<div class="col-sm-12">';
    echo '<pre id="playlistpre">';
    echo nl2br(htmlspecialchars(PLcode_above($numslides)));
    echo '<br/><br/>';

    for($i=0;$i<$numslides;$i++){
        echo "PLtime[".$i."]  = ".($slide[$i][0]*1000).";<br/>";
    }
    echo "<br/><br/>";

    for($i=0;$i<$numslides;$i++){
        echo "PLcontent[".$i."]  = '".($slide[$i][1])."';<br/>";
    }
    echo "<br/><br/>";
    echo nl2br(htmlspecialchars(PLcode_below()));
    echo '<br/></pre></div></div>';
    
    /*
    $row1 = '<div class="row">';
    $row2 = '</div>';
    $ta1 = '<textarea cols="50" rows="4">';
    $ta2 = '</textarea>';
    $sp1 = '<span style="font-size:75px;">';
    $sp2 = '</span>';
    echo $row1.$sp1.'1'.$sp2.$ta1.rawurlencode($ss1).$ta2.$row2;
    echo $row1.$sp1.'2'.$sp2.$ta1.rawurlencode($ss2).$ta2.$row2;
    echo $row1.$sp1.'3'.$sp2.$ta1.rawurlencode($ss3).$ta2.$row2;
    echo $row1.$sp1.'4'.$sp2.$ta1.rawurlencode($ss4).$ta2.$row2;
    echo $row1.$sp1.'5'.$sp2.$ta1.rawurlencode($ss5).$ta2.$row2;
    echo $row1.$sp1.'6'.$sp2.$ta1.rawurlencode($ss6).$ta2.$row2;
    */
    ?>
</div>
<div class="col-xs-4 col-xs-offset-4">
    <button id="selectallbutton" onclick="selectOutput('playlistpre');"
            class="btn btn-info btn-lg btn-block f-nova">Select All</button>
</div>


<!--/////end:(BODY)/////-->


<!-- ==========JS=========== -->
<!--##JQUERY-->
<script src="src/js/jquery-1.11.3.min.js" type="text/javascript"></script>
<!--##BOOTSTRAP_3.3-->
<script src="src/bootstrap/js/bootstrap.min.js"></script>
<script>

    function selectOutput( containerid ) {

        var node = document.getElementById( containerid );

        if ( document.selection ) {
            var range = document.body.createTextRange();
            range.moveToElementText( node  );
            range.select();
        } else if ( window.getSelection ) {
            range = document.createRange();
            range.selectNodeContents( node );
            window.getSelection().removeAllRanges();
            window.getSelection().addRange( range );
        }
    }

</script>
</body>
</html>