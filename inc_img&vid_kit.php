<?php
$thead  = '<html><head><style>';
//$thead .= 'body,html,table,td,div,video,img,p,*{cursor:none;}';
$thead .= 'body {background-color:#000000;border:none;color:#ffffff;overflow:hidden;}';
$thead .= '</style></head><body>';
$tfoot = '</body></html>';

/*[0]TIME, [1]URL, [2]PATH, [3]bgsize*/
function mk_img($arg){
    return <<<HTML
<html><head><style>/*body,html,table,td,div,video,img,p,*{cursor:none;}*/
body {border:none;overflow:hidden;}
body, html {background-color:#ffffff; color:#000;
background-image:url('$arg[2]'), url('$arg[1]$arg[2]');
background-repeat:no-repeat;background-position:center;height:100%; width:100%;padding:0; margin:0;
-webkit-background-size: $arg[3];-moz-background-size: $arg[3];
-o-background-size: $arg[3];background-size: $arg[3];}
p{margin:0;padding:0;}
</style></head><body>
</body></html>
HTML;
}
/*[0]TIME, [1]URL, [2]PATH,              [3]vIURL, [4]vIPATH, [5]VOLUME*/
function mk_vid( $arg ){
    $output = '<html><head><style>';
//    $output .= 'body,html,table,td,div,video,img,p,*{cursor:none;}';
    $output .= 'body {overflow:hidden; background-color:#fff; border:none; color:#000; margin:0; padding:0; height:100%';
    if($arg[3]!==Null && $arg[4]!==Null):
    $output .= 'background-image:url('.$arg[4].'), url('.$arg[3].$arg[4].');';
	$output .= 'background-repeat:no-repeat; padding:0; height:100%;';
    endif;
    $output .= <<<HTML
    }</style></head><body>
<video id="vid" style="position:absolute; top:0; left:0; width:100%; height:100%;">
<source src="$arg[2]">
<source src="$arg[1]$arg[2]">
</video>
HTML;
    if($arg[5]!==Null) {
        $output .= '<script type="text/javascript">';
        $output .= 'var video = document.getElementById("vid");';
        $output .= 'video.onplay = function(){video.volume = ' . $arg[5] . ';}';
        $output .= '</script>';
    }

    $output .= '</body></html>';
    return $output;
}
//function mk_vid($v_url, $v_filename, $i_url=Null, $i_filename=Null ){
//    $output = '<html><head><style>';
////    $output .= 'body,html,table,td,div,video,img,p,*{cursor:none;}';
//    $output .= 'body {overflow:hidden; background-color:#fff; border:none; color:#000; margin:0; padding:0; height:100%';
//    if($i_url!==Null && $i_filename!==Null):
//    $output .= 'background-image:url('.$i_filename.'), url('.$i_url.$i_filename.');';
//	$output .= 'background-repeat:no-repeat; padding:0; height:100%;';
//    endif;
//    $output .= '}</style></head><body>';
//    $output .= '<!--video style="position:absolute; top:0; left:0; width:100%; height:100%;">';
//    $output .= '<source src="'.$v_filename.'">';
//    $output .= '<source src="'.$v_url.$v_filename.'">';
//    $output .= '</video-->';
//    if($i_url!==Null && $i_filename!==Null):
//        $output .= '<script type="text/javascript">';
//        $output .= 'var video = document.getElementById("vid");';
//        $output .= 'video.onplay = function(){video.volume = 0.3;}';
//        $output .= '</script>';
//    endif;
//
//    $output .= '</body></html>';
//    return $output;
//}
?>