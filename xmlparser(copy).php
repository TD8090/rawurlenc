<!DOCTYPE html><html lang="en"><head><meta charset="utf-8"><title>VI-xmlParser</title><meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link href="src/bootstrap/css/bootstrap.darkly.min.css" rel="stylesheet">
    <link href="src/css/styles.css" rel="stylesheet">
</head>

<body>
<div class="container-fluid">
    <div id="Header" class="row">
        <div class="" id="viheader" style="-moz-user-select: none; -webkit-user-select: none;
         -ms-user-select:none; user-select:none;-o-user-select:none;"
             unselectable="on" onselectstart="return false;" onmousedown="return false;">
            Vi XML Parser
        </div>
    </div>
    <div id="Input" class="row">
        <form method='get'>
            <div class="col-xs-12">
                <div class="input-group">
                    <div class="input-group-btn">
                        <button tabindex="-1" id="readXML" type="submit" class="btn btn-primary" name="readXML">Read XML</button>
                        <button tabindex="-1" data-toggle="dropdown" class="btn btn-default dropdown-toggle" type="button">
                            <span class="caret"></span>
                            <span class="sr-only">Toggle Dropdown</span>
                        </button>
                        <ul class="dropdown-menu" id="locationselector">
                            <!--                            <li><button type="button" class="locselect btn btn-primary btn-xs btn-block f-nova"
                                                                    value="http://campusdining.compass-usa.com/uncg/Pages/SignageXML.aspx?location=Fountain%20View">
                                                                Fountain View</button></li>
                                                        <li><button type="button" class="locselect btn btn-primary btn-xs btn-block f-nova"
                                                                    value="http://morrisondining.compass-usa.com/sentara/Pages/SignageXML.aspx?location=Mountain%20View%20Cafe">
                                                                Mountain View Cafe</button></li>
                            -->
                            <?php
$rawlog = file_get_contents('xmlparser_urls.json');
$decoded = json_decode($rawlog, true);
//    echo "<br/>>>decoded<<<br/>";print_r( $decoded );echo "<br/>>>END:decoded<<<br/>";
                            $itemcount = count($decoded);
                            //    echo "<br/>>>itemcount<<<br/>";print_r( $itemcount );echo "<br/>>>END:itemcount<<<br/>";
                            /** SORT BEFORE FOREACH */
                            //$decoded = array_multisort($decoded, SORT_ASC);
                            foreach ($decoded as $key => $val) {
                            $btnloc = $key;
                            $btnbrand = $decoded[$key]["brand"];
                            $btnorg = $decoded[$key]["org"];
                            $btnurl = $decoded[$key]["url"];
                            //        echo "<br/>>>btnurl<<<br/>";print_r( $btnurl );echo "<br/>>>END:btnurl<<<br/>";
                            echo <<<EOT
                            <li>
                                <button type="button" class="locselect btn btn-primary btn-xs btn-block f-nova" value="$btnurl">
                                    ( $btnbrand )  <span style="font-weight:900;font-size:14px;">$btnloc</span>  ( $btnorg  )
                                </button>
                            </li>
EOT;
                            }
                            ?>
                            <li class="divider"></li>
                        </ul>
                    </div>
                    <input id="inputarea" type="text" class="form-control" name="pastedURL"
                           onchange="checkinputarea();" onkeyup="this.onchange();"
                           onpaste="this.onchange();" oninput="this.onchange();"
                           placeholder="...XML address..." />
                </div>
            </div>
        </form>
    </div>
    <div id="NinjaOutput" class="row">
        <?php
if(isset($_GET['readXML'])) {
    $simpxml = simplexml_load_file($_GET['pastedURL'], 'SimpleXMLElement', LIBXML_NOWARNING);
    //$xml = simplexml_load_file('http://campusdining.compass-usa.com/uncg/Pages/SignageXML.aspx?location=Fountain%20View');
    $pasted_url = $_GET['pastedURL'];
    if (!$simpxml) {//error message
      echo "<div class='col-xs-8 col-xs-offset-2 text-center alert alert-info' style='opacity:.8;' role='alert'>...Camelot! ...Camelot~!  ...CAMELOT!!<br/>(it's only an error...) <br/>Check out the URL:</div>";
    echo "<div class='col-xs-8 col-xs-offset-2 text-center alert alert-info' id='successlinkdiv'><a href='".$pasted_url."' id='successlink'>$pasted_url</a></div>";
    } else {

    $button_saveloc = <<<EOT
    <div class='col-sm-12'>
        <div id='successlinkdiv'><a href='$pasted_url' id='successlink'>$pasted_url</a></div>
        <button class="btn btn-success btn-xs" id="locsave" role="button">
            <span class="glyphicon glyphicon-plus" aria-hidden="true"></span>
        </button>
        <span class="panel panel-success" style="display:none; color:#00ffbd;" id="locsave_response"></span>
    </div>
EOT;
    echo $button_saveloc;
    $domx = new DOMDocument("1.0");
    $domx->preserveWhiteSpace = false;
    $domx->formatOutput = true;
    $domx->loadXML($simpxml->asXML()); //domx is now xml object
    $domxText = print_r($domx, true);
    $formatted =  "<pre id='pretag' class='hide'>" . htmlentities($domx->saveXML()) . "</pre>";
    echo $formatted;
    }
    }
    ?>
</div>
<div id="Tree">

</div>
<div id="JsOutput" class="row">
</div>


</div>
<!--/////end:(BODY)/////-->

<br/><br/><br/>

<!-- ==========JS=========== -->
<!--##JQUERY-->
<script src="src/js/jquery-1.11.3.min.js" type="text/javascript"></script>
<!--##BOOTSTRAP_3.3-->
<script src="src/bootstrap/js/bootstrap.min.js"></script>
<script>

    var log = function (r) {console.log(r)};
    var processXML = function(){
        var fromNinja, venues = [], periods = [], stations = [], menuitems = []
                , _venuehtml = [], _periodhtml = [], _stationhtml = []
                , venuenames = [], periodnames = [], stationnames = [], menuitemnames = [], i, j, k, l
                , br = '<br/>'
                , h1_open = '<strong style="font-size:22px; text-decoration: underline;">'
                , h1_close = '</strong>'
                , h2_open = '<span style="font-size:20px;">'
                , h2_close = '</span>'
                , icon_home = '   <div class="icon_home"><span class="glyphicon glyphicon-home" aria-hidden="true"></span></div> '
                , icon_time = '<span class="glyphicon glyphicon-time" style="margin-left: 31px;" aria-hidden="true"></span> '
                , icon_disabled = '<button class="btn btn-default btn-xs" disabled="disabled"><span class="glyphicon glyphicon-ban-circle" aria-hidden="true"></span></button> '
                , icon_cutlery = '<span class="glyphicon glyphicon-cutlery" aria-hidden="true"></span>'
                , stationspan_open = '<span class="f-stations">'
                , stationspan_close = '</span>'
                , itemspan_open = '<span class="menuitems">'
                , itemspan_close = '</span>'
                , expand_href_open1 = '<a class="btn btn-default btn-xs" role="button" data-toggle="collapse" href="#'
                , expand_href_open2 = '" aria-expanded="false" aria-controls="'
                , expand_button_open1 = '<button class="btn btn-default btn-xs" role="button" data-toggle="collapse" data-target="#'
                , expand_button_open2 = '" aria-expanded="false" aria-controls="'
                , expand_button_open3 = '">'
                , expand_href_close = '</a>'
                , expand_button_close = '</button> '
                , itemz_expand_open1 = '<div class="collapse" id="'
                , itemz_expand_open2 = '"><div class="itemz panel panel-info">'
                , expand_close = '</div></div>'
                , checkbox = '<button type="button" class="btn btn-xs btn-default btncheck" data-toggle="button" aria-pressed="false" autocomplete="off"><span class="glyphicon glyphicon-ok" aria-hidden="true"></span></button>'
                , tog_comments = '<button type="button" id="btntogcomment" onClick="showComments(this)" class="btn btn-xs btn-default" data-toggle="button" aria-pressed="false" autocomplete="off"><span class="glyphicon glyphicon-option-horizontal" aria-hidden="true"></span></button>'
                , stationcomments = '<input size="3" class="stationcomments" style="display:none;" type="text" onchange="sizer(this);" onkeyup="this.onchange();" onpaste="this.onchange();" oninput="this.onchange();" />'
                , Tree = $('#Tree')
                , rgx_venues = /(&lt;Venue [\s\S]*?&lt;\/Venue&gt;)/g
                , rgx_venuenames = /&lt;Venue name="([\w\W]*?)"\/?&gt;/
                , rgx_periods = /(&lt;MealPeriod[\s\S]*?&lt;\/MealPeriod\/?&gt;)/g
                , rgx_periodnames = /&lt;MealPeriod name="([\w\W]*?)"\/?&gt;/
                , rgx_stations = /(&lt;MealStation name="(?:[^\r\n]|\r(?!\n))*?"&gt;[\S\s]*?&lt;\/MealStation&gt;|&lt;MealStation name="(?:[^\r\n]|\r(?!\n))*?"\/&gt;)/g
                , rgx_stations_nochildren = /&lt;MealStation name="(?:[^\r\n]|\r(?!\n))*?"\/&gt;/
                , rgx_stationnames = /&lt;MealStation name="([\w\W]*?)"\/?&gt;/
                , rgx_menuitems = /(&lt;MenuItem[\s\S]*?&lt;\/MenuItem&gt;)/g
                , rgx_menuitemnames = /&lt;MenuItem name="([\w\W]*?)" desc/
                ;
        fromNinja = document.getElementById('NinjaOutput').innerHTML;
        //document.getElementById('JsOutput').innerHTML = fromNinja;

        var venue_match = fromNinja.match( rgx_venues );
        for(i=0;i<venue_match.length;i++){
            periods = venue_match[i].match( rgx_periods );
            venuenames[i] = venue_match[i].match( rgx_venuenames );
            _venuehtml[i] = '';
            _venuehtml[i] = icon_home + h1_open + venuenames[i][1]+ h1_close + tog_comments + br;
            Tree.append(_venuehtml[i]);

            //periods[] now has 5 mealperiod enclosures
            for(j=0;j<periods.length;j++){
                //current mealperiod name to cur_periodname[i]
                periodnames[j] = periods[j].match( rgx_periodnames );
                //append current periodname to output
                _periodhtml[j] = '';
                _periodhtml[j] += icon_time + h2_open + periodnames[j][1]+ h2_close + br;

                Tree.append(_periodhtml[j]);

                //for this mealperiod, store mealstation enclosures
                stations = periods[j].match( rgx_stations );
                for(k=0;k<stations.length;k++){
                    //current mealstation name to cur_stationname[j]
                    stationnames[k] = stations[k].match( rgx_stationnames );
                    //BUTTON before station name to open stations menu items

                    /*If stations[k] has no children, display station with ban-circle icon and exit loop*/
                    if( rgx_stations_nochildren.test(stations[k]) ) {
                        _stationhtml[k] = '';
                        _stationhtml[k] += icon_disabled + stationspan_open + stationnames[k][1] + stationspan_close + br;
                    }else{
                        _stationhtml[k] = '';
                        _stationhtml[k] += checkbox;
                        _stationhtml[k] += expand_button_open1 + stationnames[k][1].replace(/[^a-zA-Z0-9]+/g, '')+i+j+k;
                        _stationhtml[k] += expand_button_open2 + stationnames[k][1].replace(/[^a-zA-Z0-9]+/g, '')+i+j+k;
                        _stationhtml[k] += expand_button_open3;
                        _stationhtml[k] += icon_cutlery + expand_button_close;

                        _stationhtml[k] += stationspan_open + stationnames[k][1] + stationspan_close +stationcomments+br;

                        //for this mealstation, store menuitem enclosures
                        menuitems = stations[k].match(rgx_menuitems);

                        _stationhtml[k] += itemz_expand_open1;
                        _stationhtml[k] += stationnames[k][1].replace(/[^a-zA-Z0-9]+/g, '')+i+j+k;
                        _stationhtml[k] += itemz_expand_open2;
                        for (l = 0; l < menuitems.length; l++) {
                            //for this menu item, name to menuitemnames[l]
                            menuitemnames[l] = menuitems[l].match(rgx_menuitemnames);
                            //add to string all the menuitem names in this(\\k) station
                            _stationhtml[k] += itemspan_open + menuitemnames[l][1] + itemspan_close + br;
                            //for this mealstation, store menuitem enclosures
                        }
                        _stationhtml[k] += expand_close;
                    }
                    Tree.append(_stationhtml[k]);
                }
            }
        }
    };//END: processXML()
    var checkinputarea = function() {
        var empty = false;
        if ($('#inputarea').val() == '') {
            empty = true;
        }
        if (empty) {
            $('#readXML').attr('disabled', 'disabled');
        } else {
            $('#readXML').removeAttr('disabled');
        }
    };
    var addURL = function(){
        var rgx_brand = /http:\/\/([\s\S]*)\.compass-usa\.com\/[\s\S]*.aspx/i;
        var rgx_org = /http:\/\/[\s\S]*\.compass-usa\.com\/([\s\S]*)\/Pages/i;
        var rgx_loc = /http:\/\/[\s\S]*\.compass-usa\.com\/[\s\S]*\/Pages\/SignageXML\.aspx\?location=([\s\S]*)/i;
        var exp_url = $('#successlink').attr('href');
        var thecomments = '';
        var match_brand = exp_url.match(rgx_brand);
        var match_org = exp_url.match(rgx_org);
        var match_loc = exp_url.match(rgx_loc);
        var exp_loc = decodeURIComponent(match_loc[1].toString());
        var exp_brand = match_brand[1].toString();
        var exp_org = match_org[1].toString();
        var venuecomments = [];
        var $this, obj, text, $input;
        $(".f-stations").each(function(){
            $this = $(this);
            $input = $this.next(".stationcomments");
            if ($input.val().length > 0 ){
                text = $this.text();
                log(text);
                obj = {};
                obj[text] = $input.val();
                log(obj);
                venuecomments.push(obj);
                log("f-station done!")
            }
        });
//obj{}
        //log(exp_brand);log(exp_org);log(exp_loc);
        //http://stackoverflow.com/questions/19970301/convert-javascript-object-or-array-to-json-for-ajax-data
        var jsonObject = {};
        jsonObject[exp_loc] =
        {
            "brand":exp_brand,
            "org":exp_org,
            "url":exp_url,
            "comments": venuecomments
        };
        log(jsonObject);
        log(JSON.stringify(jsonObject));
        log("poop");
        //jsonObject.metros[index] = JSON.stringify(graph.getVertex(index).getData());
        $.ajax({
            type: "POST", url: "xmlparser_urls.php",
            data: {//http://stackoverflow.com/questions/3921520/writing-json-object-to-json-file-on-server
                json : JSON.stringify(jsonObject)
            },
            dataType : 'json',//json expected in response, comment out all debugging in php.
            success: function(res){
                $('#locsave_response').html(res).show().css('fontsize', '12px').css('font-family','courier');
                //log(res);log("that was the res");
            },
            error: function(jqXHR, textStatus, errorThrown) {
                log("ajax unsuccessful" +"+"+ jqXHR +"+"+ textStatus +"+"+ errorThrown);
                log("and the res text...."+ jqXHR.responseText);
            }
        });
    };
    var sizer = function(e){
        var size = parseInt($(e).attr('size'));
        var chars = $(e).val().length;
        $(e).attr('size', chars+1);
    };
    var showComments = function(e){
        if( $( e ).hasClass( "active" ) ) {
            $('.stationcomments').css({'display':'none'});
        }else{
            $('.stationcomments').css({'display':'inline-block'});
        }
    };

    $(document).ready(function (){
        checkinputarea();
        $('#readXML').attr('disabled', 'disabled');
        $('#inputarea').on('input', checkinputarea());
        //location dropdown -> fill input
        $(".locselect").click(function() {
            var txt = $(this).val();
            $('#inputarea').val('').val(function(i,val){
                return val + txt;
            });
            checkinputarea();
        });
        //comment toggle
        $('#locsave').click(addURL);
        /*    $(function(){
         $('#menu_search').keyup(function(){
         var size = parseInt($(this).attr('size'));
         var chars = $(this).val().length;
         if(chars >= size) $(this).attr('size', chars);
         });
         });*/

        /**
         *
         *
         * */

            //run processXML() when #preXML is detected as loaded
        $('#preXML').load(processXML());
    });//END: $(document).ready(function (){








</script>
</body>
</html>