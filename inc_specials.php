<?php
$specials_head = <<<HTML
<html><head><meta charset="utf-8">
<style>html, body {background-color:#000000;border:none;color:#ffffff;overflow:hidden;margin:0; height:100%; width:100%;padding:0;}</style>
    <script type="text/javascript">
        function slideShowOpac (direction, v){
            if (direction == "in") {theonethatfadesin.style.opacity = v/10;}
            else if (direction == "out"){theonethatfadesout.style.opacity = v/10;}
        }
        function slideShowFadeIn (obj){theonethatfadesin = obj;
            for (var i=0;i<=10;i++) {
                setTimeout('slideShowOpac("in", '+i+')', 50*i);
            }
        }
        function slideShowFadeOut (obj){
            if (obj.style.opacity > 0) {theonethatfadesout = obj;
                for (var i=10, j=0; i >= 0; i--) {
                    j++;setTimeout('slideShowOpac("out", '+i+')', 50*j);
                }
            }
        }
        function switchPotato () {
            for (var i=0;i<potatoes.length;i++){
                if (i == thisPotato) {slideShowFadeIn(potatoes[i]);}
                else                 {slideShowFadeOut(potatoes[i]);}
            }
            thisPotato++;
            if (thisPotato != potatoes.length) window.switchSlide = setTimeout(function(){switchPotato();}, 5000);
        }
        function startSlideshow () {
            if (potatoes.length >= 1) {switchPotato();}
            else {/*there are no slides to show*/}
        }
    </script>
</head><body>
<style type="text/css">
        html, body {background-color:#000000;border:none;color:#ffffff;overflow:hidden;margin:0; height:100%; width:100%;padding:0;}
    body {font-family:'Helvetica',sans-serif;text-align: center;
        background-image: url('ABC_Template5.jpg'), url('http://vi-digital.com/lib/vicorporatemenus/ABC-Corp/ABC_Template5.jpg');
        background-repeat:no-repeat; background-color:#fff; color: #000; }
    td {font-weight:bold; vertical-align:top;text-align: right;
        font-size: 47px;line-height: 70px;width: 700px;}
    .slide{position: absolute;top: 200px;left: 80px;}
    .imgcell {text-align: center;width:400px;}
    .slide img{height:500px;width:auto;}
    #header {width: 1280px; padding-top: 40px; text-transform: uppercase;
        font-family:  'Helvetica', sans-serif;text-align: center;
        font-weight: 600;font-size: 90px;line-height:90px;color: #fff; }
    .large { font-size: 64px; line-height: 64px; text-align: left; }
    .red   { color: #cf0d2f; }
</style>
<div id="header">AUGUST Specials</div>
HTML;
$r1 = <<<HTML
<div id="rotatoPotato">
    <table border="0" class="slide" style="opacity: 0;"><tbody><tr><td class="large red">Jim Beam Black</td><td rowspan="5" class="imgcell"><img alt="image 1" onerror="this.src='http://vi-digital.com/lib/abc/ABC-Cherry/23060_Jim_Beam_Black.png';" src="23060_Jim_Beam_Black.png" /></td><tr><td>.75L</td></tr><tr><td>Regular Price 26.95</td></tr><tr><td>Savings 4.00</td></tr><tr><td class="red">Sale Price 22.95</td></tr></tbody></table>
    <table border="0" class="slide" style="opacity: 0;"><tbody><tr><td class="large red">Knob Creek</td><td rowspan="5" class="imgcell"><img alt="image 2" onerror="this.src='http://vi-digital.com/lib/abc/ABC-Cherry/27015_Knob_Creek.png';" src="27015_Knob_Creek.png" /></td><tr><td>1.75L</td></tr><tr><td>Regular Price 71.95</td></tr><tr><td>Savings 7.00</td></tr><tr><td class="red">Sale Price 64.95</td></tr></tbody></table>
    <table border="0" class="slide" style="opacity: 0;"><tbody><tr><td class="large red">Bulleit Bourbon</td><td rowspan="5" class="imgcell"><img alt="image 3" onerror="this.src='http://vi-digital.com/lib/abc/ABC-Cherry/27026_Bulleit_Bourbon.png';" src="27026_Bulleit_Bourbon.png" /></td><tr><td>.75L</td></tr><tr><td>Regular Price 31.95</td></tr><tr><td>Savings 3.00</td></tr><tr><td class="red">Sale Price 28.95</td></tr></tbody></table>
    <table border="0" class="slide" style="opacity: 0;"><tbody><tr><td class="large red">Bird Dog Peach Whiskey(6)</td><td rowspan="5" class="imgcell"><img alt="image 4" onerror="this.src='http://vi-digital.com/lib/abc/ABC-Cherry/27043_Bird_Dog_Peach_Whiskey6.png';" src="27043_Bird_Dog_Peach_Whiskey6.png" /></td><tr><td>.75L</td></tr><tr><td>Regular Price 18.95</td></tr><tr><td>Savings 3.00</td></tr><tr><td class="red">Sale Price 15.95</td></tr></tbody></table>
    <table border="0" class="slide" style="opacity: 0;"><tbody><tr><td class="large red">Henry McKenna Single Barrel(6)</td><td rowspan="5" class="imgcell"><img alt="image 5" onerror="this.src='http://vi-digital.com/lib/abc/ABC-Cherry/27115_Henry_McKenna_Single_Barrel6.png';" src="27115_Henry_McKenna_Single_Barrel6.png" /></td><tr><td>.75L</td></tr><tr><td>Regular Price 28.95</td></tr><tr><td>Savings 4.00</td></tr><tr><td class="red">Sale Price 24.95</td></tr></tbody></table>
</div>
HTML;
$r2 = <<<HTML
<div id="rotatoPotato">
    <table border="0" class="slide" style="opacity: 0;"><tbody><tr><td class="large red">Woodford Reserve</td><td rowspan="5" class="imgcell"><img alt="image 1" onerror="this.src='http://vi-digital.com/lib/abc/ABC-Cherry/27127_Woodford_Reserve.png';" src="27127_Woodford_Reserve.png" /></td><tr><td>.75L</td></tr><tr><td>Regular Price 37.95</td></tr><tr><td>Savings 5.00</td></tr><tr><td class="red">Sale Price 32.95</td></tr></tbody></table>
    <table border="0" class="slide" style="opacity: 0;"><tbody><tr><td class="large red">Grand MacNish 12Y</td><td rowspan="5" class="imgcell"><img alt="image 2" onerror="this.src='http://vi-digital.com/lib/abc/ABC-Cherry/35492_Grand_MacNish_12Y.png';" src="35492_Grand_MacNish_12Y.png" /></td><tr><td>.75L</td></tr><tr><td>Regular Price 24.95</td></tr><tr><td>Savings 4.00</td></tr><tr><td class="red">Sale Price 20.95</td></tr></tbody></table>
    <table border="0" class="slide" style="opacity: 0;"><tbody><tr><td class="large red">Forty Creek</td><td rowspan="5" class="imgcell"><img alt="image 3" onerror="this.src='http://vi-digital.com/lib/abc/ABC-Cherry/37798_Forty_Creek.png';" src="37798_Forty_Creek.png" /></td><tr><td>1.75L</td></tr><tr><td>Regular Price 42.95</td></tr><tr><td>Savings 5.00</td></tr><tr><td class="red">Sale Price 37.95</td></tr></tbody></table>
    <table border="0" class="slide" style="opacity: 0;"><tbody><tr><td class="large red">Tullamore Dew 12Y(6)</td><td rowspan="5" class="imgcell"><img alt="image 4" onerror="this.src='http://vi-digital.com/lib/abc/ABC-Cherry/40118_Tullamore_Dew_12Y6.png';" src="40118_Tullamore_Dew_12Y6.png" /></td><tr><td>.75L</td></tr><tr><td>Regular Price 49.95</td></tr><tr><td>Savings 5.00</td></tr><tr><td class="red">Sale Price 44.95</td></tr></tbody></table>
    <table border="0" class="slide" style="opacity: 0;"><tbody><tr><td class="large red">American Harvest Organic Spirit(6)</td><td rowspan="5" class="imgcell"><img alt="image 5" onerror="this.src='http://vi-digital.com/lib/abc/ABC-Cherry/43304_American_Harvest_Organic_Spirit6.png';" src="43304_American_Harvest_Organic_Spirit6.png" /></td><tr><td>.75L</td></tr><tr><td>Regular Price 25.95</td></tr><tr><td>Savings 6.00</td></tr><tr><td class="red">Sale Price 19.95</td></tr></tbody></table>
</div>
HTML;
$r3 = <<<HTML
<div id="rotatoPotato">
    <table border="0" class="slide" style="opacity: 0;"><tbody><tr><td class="large red">FireFly Sweet Tea</td><td rowspan="5" class="imgcell"><img alt="image 1" onerror="this.src='http://vi-digital.com/lib/abc/ABC-Cherry/43572_FireFly_Sweet_Tea.png';" src="43572_FireFly_Sweet_Tea.png" /></td><tr><td>1.75L</td></tr><tr><td>Regular Price 37.95</td></tr><tr><td>Savings 7.00</td></tr><tr><td class="red">Sale Price 30.95</td></tr></tbody></table>
    <table border="0" class="slide" style="opacity: 0;"><tbody><tr><td class="large red">Rain</td><td rowspan="5" class="imgcell"><img alt="image 2" onerror="this.src='http://vi-digital.com/lib/abc/ABC-Cherry/44023_Rain.png';" src="44023_Rain.png" /></td><tr><td>1.75L</td></tr><tr><td>Regular Price 29.95</td></tr><tr><td>Savings 3.00</td></tr><tr><td class="red">Sale Price 26.95</td></tr></tbody></table>
    <table border="0" class="slide" style="opacity: 0;"><tbody><tr><td class="large red">Deep Eddy Lemon</td><td rowspan="5" class="imgcell"><img alt="image 3" onerror="this.src='http://vi-digital.com/lib/abc/ABC-Cherry/44317_Deep_Eddy_Lemon.png';" src="44317_Deep_Eddy_Lemon.png" /></td><tr><td>.75L</td></tr><tr><td>Regular Price 18.95</td></tr><tr><td>Savings 3.00</td></tr><tr><td class="red">Sale Price 15.95</td></tr></tbody></table>
    <table border="0" class="slide" style="opacity: 0;"><tbody><tr><td class="large red">Stolichnaya 80</td><td rowspan="5" class="imgcell"><img alt="image 4" onerror="this.src='http://vi-digital.com/lib/abc/ABC-Cherry/46595_Stolichnaya_80.png';" src="46595_Stolichnaya_80.png" /></td><tr><td>.75L</td></tr><tr><td>Regular Price 21.95</td></tr><tr><td>Savings 4.00</td></tr><tr><td class="red">Sale Price 17.95</td></tr></tbody></table>
    <table border="0" class="slide" style="opacity: 0;"><tbody><tr><td class="large red">Double Cross Vodka(6)</td><td rowspan="5" class="imgcell"><img alt="image 5" onerror="this.src='http://vi-digital.com/lib/abc/ABC-Cherry/46898_Double_Cross_Vodka6.png';" src="46898_Double_Cross_Vodka6.png" /></td><tr><td>.75L</td></tr><tr><td>Regular Price 37.95</td></tr><tr><td>Savings 8.00</td></tr><tr><td class="red">Sale Price 29.95</td></tr></tbody></table>
</div>
HTML;
$r4 = <<<HTML
<div id="rotatoPotato">
    <table border="0" class="slide" style="opacity: 0;"><tbody><tr><td class="large red">Gosling's Black Seal</td><td rowspan="5" class="imgcell"><img alt="image 1" onerror="this.src='http://vi-digital.com/lib/abc/ABC-Cherry/49125_Goslings_Black_Seal.png';" src="49125_Goslings_Black_Seal.png" /></td><tr><td>.75L</td></tr><tr><td>Regular Price 17.95</td></tr><tr><td>Savings 3.00</td></tr><tr><td class="red">Sale Price 14.95</td></tr></tbody></table>
    <table border="0" class="slide" style="opacity: 0;"><tbody><tr><td class="large red">Capt. Morgan Private Stock</td><td rowspan="5" class="imgcell"><img alt="image 2" onerror="this.src='http://vi-digital.com/lib/abc/ABC-Cherry/49229_Capt_Morgan_Private_Stock.png';" src="49229_Capt_Morgan_Private_Stock.png" /></td><tr><td>.75L</td></tr><tr><td>Regular Price 24.95</td></tr><tr><td>Savings 3.00</td></tr><tr><td class="red">Sale Price 21.95</td></tr></tbody></table>
    <table border="0" class="slide" style="opacity: 0;"><tbody><tr><td class="large red">Malibu Rum Punch Cocktail</td><td rowspan="5" class="imgcell"><img alt="image 3" onerror="this.src='http://vi-digital.com/lib/abc/ABC-Cherry/51926_Malibu_Rum_Punch_Cocktail.png';" src="51926_Malibu_Rum_Punch_Cocktail.png" /></td><tr><td>1.75L</td></tr><tr><td>Regular Price 17.95</td></tr><tr><td>Savings 3.00</td></tr><tr><td class="red">Sale Price 14.95</td></tr></tbody></table>
    <table border="0" class="slide" style="opacity: 0;"><tbody><tr><td class="large red">Twenty Grand Rose(6)</td><td rowspan="5" class="imgcell"><img alt="image 4" onerror="this.src='http://vi-digital.com/lib/abc/ABC-Cherry/56882_Twenty_Grand_Rose6.png';" src="56882_Twenty_Grand_Rose6.png" /></td><tr><td>.75L</td></tr><tr><td>Regular Price 29.95</td></tr><tr><td>Savings 4.00</td></tr><tr><td class="red">Sale Price 25.95</td></tr></tbody></table>
    <table border="0" class="slide" style="opacity: 0;"><tbody><tr><td class="large red">DeKuyper O3</td><td rowspan="5" class="imgcell"><img alt="image 5" onerror="this.src='http://vi-digital.com/lib/abc/ABC-Cherry/61768_DeKuyper_O3.png';" src="61768_DeKuyper_O3.png" /></td><tr><td>.75L</td></tr><tr><td>Regular Price 19.95</td></tr><tr><td>Savings 3.00</td></tr><tr><td class="red">Sale Price 16.95</td></tr></tbody></table>
</div>
HTML;
$r5 = <<<HTML
<div id="rotatoPotato">
    <table border="0" class="slide" style="opacity: 0;"><tbody><tr><td class="large red">Wild Turkey American Honey</td><td rowspan="5" class="imgcell"><img alt="image 1" onerror="this.src='http://vi-digital.com/lib/abc/ABC-Cherry/61950_Wild_Turkey_American_Honey.png';" src="61950_Wild_Turkey_American_Honey.png" /></td><tr><td>.75L</td></tr><tr><td>Regular Price 23.95</td></tr><tr><td>Savings 3.00</td></tr><tr><td class="red">Sale Price 20.95</td></tr></tbody></table>
    <table border="0" class="slide" style="opacity: 0;"><tbody><tr><td class="large red">Caravella Limoncello(6)</td><td rowspan="5" class="imgcell"><img alt="image 2" onerror="this.src='http://vi-digital.com/lib/abc/ABC-Cherry/63573_Caravella_Limoncello6.png';" src="63573_Caravella_Limoncello6.png" /></td><tr><td>.75L</td></tr><tr><td>Regular Price 22.95</td></tr><tr><td>Savings 4.00</td></tr><tr><td class="red">Sale Price 18.95</td></tr></tbody></table>
    <table border="0" class="slide" style="opacity: 0;"><tbody><tr><td class="large red">Leblon Cachaca(6)</td><td rowspan="5" class="imgcell"><img alt="image 3" onerror="this.src='http://vi-digital.com/lib/abc/ABC-Cherry/63805_Leblon_Cachaca6.png';" src="63805_Leblon_Cachaca6.png" /></td><tr><td>.75L</td></tr><tr><td>Regular Price 26.95</td></tr><tr><td>Savings 3.00</td></tr><tr><td class="red">Sale Price 23.95</td></tr></tbody></table>
    <table border="0" class="slide" style="opacity: 0;"><tbody><tr><td class="large red">Lunazul Blanco</td><td rowspan="5" class="imgcell"><img alt="image 4" onerror="this.src='http://vi-digital.com/lib/abc/ABC-Cherry/65047_Lunazul_Blanco.png';" src="65047_Lunazul_Blanco.png" /></td><tr><td>1.75L</td></tr><tr><td>Regular Price 34.95</td></tr><tr><td>Savings 5.00</td></tr><tr><td class="red">Sale Price 29.95</td></tr></tbody></table>
    <table border="0" class="slide" style="opacity: 0;"><tbody><tr><td class="large red">Sauza 901 Silver Tequila(6)</td><td rowspan="5" class="imgcell"><img alt="image 5" onerror="this.src='http://vi-digital.com/lib/abc/ABC-Cherry/65080_Sauza_901_Silver_Tequila6.png';" src="65080_Sauza_901_Silver_Tequila6.png" /></td><tr><td>.75L</td></tr><tr><td>Regular Price 29.95</td></tr><tr><td>Savings 5.00</td></tr><tr><td class="red">Sale Price 24.95</td></tr></tbody></table>
</div>
HTML;
$r6 = <<<HTML
<div id="rotatoPotato">
    <table border="0" class="slide" style="opacity: 0;"><tbody><tr><td class="large red">Espolon Blanco(6)</td><td rowspan="5" class="imgcell"><img alt="image 1" onerror="this.src='http://vi-digital.com/lib/abc/ABC-Cherry/65165_Espolon_Blanco6.png';" src="65165_Espolon_Blanco6.png" /></td><tr><td>.75L</td></tr><tr><td>Regular Price 26.95</td></tr><tr><td>Savings 3.00</td></tr><tr><td class="red">Sale Price 23.95</td></tr></tbody></table>
    <table border="0" class="slide" style="opacity: 0;"><tbody><tr><td class="large red">Two Fingers Gold</td><td rowspan="5" class="imgcell"><img alt="image 2" onerror="this.src='http://vi-digital.com/lib/abc/ABC-Cherry/65349_Two_Fingers_Gold.png';" src="65349_Two_Fingers_Gold.png" /></td><tr><td>1.75L</td></tr><tr><td>Regular Price 25.95</td></tr><tr><td>Savings 3.00</td></tr><tr><td class="red">Sale Price 22.95</td></tr></tbody></table>
    <table border="0" class="slide" style="opacity: 0;"><tbody><tr><td class="large red">Cabo Wabo Reposado(6)</td><td rowspan="5" class="imgcell"><img alt="image 3" onerror="this.src='http://vi-digital.com/lib/abc/ABC-Cherry/65359_Cabo_Wabo_Reposado6.png';" src="65359_Cabo_Wabo_Reposado6.png" /></td><tr><td>.75L</td></tr><tr><td>Regular Price 39.95</td></tr><tr><td>Savings 3.00</td></tr><tr><td class="red">Sale Price 36.95</td></tr></tbody></table>
    <table border="0" class="slide" style="opacity: 0;"><tbody><tr><td class="large red">Catdaddy Spiced Moonshine(6)</td><td rowspan="5" class="imgcell"><img alt="image 4" onerror="this.src='http://vi-digital.com/lib/abc/ABC-Cherry/66100_Catdaddy_Spiced_Moonshine6.png';" src="66100_Catdaddy_Spiced_Moonshine6.png" /></td><tr><td>.75L</td></tr><tr><td>Regular Price 22.95</td></tr><tr><td>Savings 3.00</td></tr><tr><td class="red">Sale Price 19.95</td></tr></tbody></table>
</div>
HTML;


$specials_foot = <<<HTML
<script type="text/javascript">
    /* HEY! If the moon were made out of spare ribs, would you eat it? I know I would..*/
    var thisPotato = 0;
    var potatoDiv = document.getElementById('rotatoPotato');
    var potatoes = potatoDiv.children;
    var theonethatfadesin, theonethatfadesout;
    window.contentType= 'slideshow';
</script></body></html>
HTML;

$ss1 = $specials_head.$r1.$specials_foot;
$ss2 = $specials_head.$r2.$specials_foot;
$ss3 = $specials_head.$r3.$specials_foot;
$ss4 = $specials_head.$r4.$specials_foot;
$ss5 = $specials_head.$r5.$specials_foot;
$ss6 = $specials_head.$r6.$specials_foot;

?>